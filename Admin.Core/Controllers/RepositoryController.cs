﻿using Admin.Core.Common.Auth;
using Admin.Core.Common.BaseModel;
using Admin.Core.Common.Output;
using Admin.Core.Controllers.Admin;
using Admin.Core.Extensions;
using Admin.Core.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Admin.Core.Controllers
{
    public abstract class RepositoryController<TEntity, TInsertInput, TUpdateInput> : AreaController
        where TEntity : Entity<long>
        where TUpdateInput : IEntity<long>
    {
        //private IFreeSql FreeSql { get; }
        //private IBaseRepository<TEntity, long> Repository { get; }

        private IRepositoryBase<TEntity> _repository { get; }
        private IUser _user { get; }

        public RepositoryController(IUser user, IRepositoryBase<TEntity> repository)
        {
            //_user = user;
            //FreeSql = freeSql;
            //Repository = FreeSql.GetRepository<TEntity, long>();         

            _user = user;
            _repository = repository;
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="input">参数model</param>
        /// <returns></returns>
        [HttpPost]
        public virtual async Task<IResponseOutput> AddAsync(TInsertInput input)
        {
            var result = await _repository.InsertAsync(input.MapTo<TEntity>());
            return ResponseOutput.Ok(result.Id);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="input">参数model</param>
        /// <returns></returns>
        [HttpPut]
        public virtual async Task<IResponseOutput> UpdateAsync(TUpdateInput input)
        {
            if (input?.Id < 1)
                return ResponseOutput.NotOk("id非法");

            var result = await _repository.UpdateAsync(input.MapTo<TEntity>());
            return ResponseOutput.Result(result > 0);
        }

        /// <summary>
        /// 软删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public virtual async Task<IResponseOutput> SoftDeleteAsync(long id)
        {
            if (id < 0) return ResponseOutput.NotOk("id非法");

            var result = await _repository.UpdateDiy.SetDto(new
            {
                IsDeleted = true,
                ModifiedUserId = _user?.Id,
                ModifiedUserName = _user?.Name
            })
                .WhereDynamic(id)
                .ExecuteAffrowsAsync();
            return ResponseOutput.Result(result > 0);
        }

        /// <summary>
        /// 批量软删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPut]
        public virtual async Task<IResponseOutput> BatchSoftDelete(long[] ids)
        {
            if (ids.Length < 1) return ResponseOutput.NotOk();

            var result = await _repository.UpdateDiy.SetDto(new
            {
                IsDeleted = true,
                ModifiedUserId = _user?.Id,
                ModifiedUserName = _user?.Name
            })
                .WhereDynamic(ids)
                .ExecuteAffrowsAsync();
            return ResponseOutput.Result(result > 0);
        }

        /// <summary>
        /// 查询单条数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual async Task<IResponseOutput> Get(long id)
        {
            if (id < 0) return ResponseOutput.NotOk("id非法");
            var result = await _repository.FindAsync(id);
            return ResponseOutput.Ok(result);
        }

        /// <summary>
        /// 判断名称是否存在
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual async Task<IResponseOutput> Exist(string name)
        {
            using (_repository.DataFilter.DisableAll())
            {
                var result = await _repository.GetAsync(t => t.KeyWords.ToLower() == name.ToLower());
                return ResponseOutput.Ok(result != null);
            }
        }

        //[HttpPost]
        //public virtual async Task<IResponseOutput> DeleteAsync(long id)
        //{
        //    if (id < 0) return ResponseOutput.NotOk();

        //    var result = await Repository.DeleteAsync(m => m.Id == id);

        //    return ResponseOutput.Result(result > 0);
        //}

        //public virtual async Task<IResponseOutput> GetPage(PageInput<TEntity> input)
        //{
        //    var key = input.Filter?.Keyword;

        //    var list = await Repository.Select
        //    .WhereIf(key.NotNull(), a => a.Keyword.Contains(key))
        //    .Count(out var total)
        //    .OrderByDescending(true, c => c.Id)
        //    .Page(input.CurrentPage, input.PageSize)
        //    .ToListAsync<TQueryOutput>();

        //    var data = new PageOutput<TQueryOutput>()
        //    {
        //        List = list,
        //        Total = total
        //    };

        //    return ResponseOutput.Ok(data);
        //}
    }
}
