﻿using Mapster;

namespace Admin.Core.Extensions
{
    public static class MapperExtensions
    {
        public static TTarget MapTo<TTarget>(this object source)
        {
            return source.Adapt<TTarget>();
        }
    }
}
